#!/bin/bash
set -e

# setup ros environment
source "/root/dev_ws/devel/setup.bash"
if [ -d "/opt/carla" ]; then
  source "/opt/carla-ros-bridge/install/setup.bash"
  source "/opt/carla/setup.bash"
else
  source "/opt/ros/$ROS_DISTRO/setup.bash"
fi

CARLA_HOST="${CARLA_HOST:-localhost}"
CARLA_PORT="${CARLA_PORT:-2000}"

echo $ROS_IP
echo $ROS_MASTER_URI

if [[ -z "$NO_CARLA" && -d "/opt/carla" ]]; then
  echo "Launching CARLA ROS bridge in background ..."
  roslaunch carla_ros_bridge carla_ros_bridge.launch timeout:=20 host:=$CARLA_HOST post:=$CARLA_PORT &
elif [[ -z "$ROS_IP" && "$ROS_MASTER_URI" == "http://localhost:11311" ]]; then  # if ROS_* variables not overriden to use external roscore
  echo "Launching standalone ROS core ..."
  roscore &
fi

sleep 5

echo "Launching Foxglove Websocket bridge in background ..."
roslaunch foxglove_bridge foxglove_bridge.launch &

exec "$@"