import {FoxgloveClient} from '@foxglove/ws-protocol'
import {parse} from '@foxglove/rosmsg'
import {MessageReader} from '@foxglove/rosmsg-serialization'
import {messageDefinitions} from './ros_msgs.gen.js'

// loading and parsing message definitions
const defs = parse(messageDefinitions)

function getMessageDef(forType) {
    const typeDef = defs.filter(e => e.name === forType)[0]
    const clonedDefs = JSON.parse(JSON.stringify(defs))
    clonedDefs[0] = typeDef
    return clonedDefs
}

function getMessageReader(forType) {
    return new MessageReader(getMessageDef(forType))
}

// initializing message readers
const messageReaders = {}
const messageTypes = [
    'sensor_msgs/CompressedImage',
]
messageTypes.forEach(t => messageReaders[t] = getMessageReader(t))

// actual client subscription and message handling
const rosClient = new FoxgloveClient({
    ws: new WebSocket(process.env.APP_FOXGLOVE_URL || 'ws://localhost:8765', [FoxgloveClient.SUPPORTED_SUBPROTOCOL])
})
const handlers = {}  // sid -> callback

rosClient.on('error', console.error)

rosClient.on('advertise', (channels) => {
        for (const channel of channels) {
            if (channel.encoding !== 'ros1') continue

            switch (channel.topic) {
                case '/sensors/camera/front_medium/image_color/compressed':
                    const subId = rosClient.subscribe(channel.id)
                    handlers[subId] = onCameraImage
                    console.log(`Subscribed to camera images under ID ${subId}`)
                    break
            }
        }
    })

rosClient.on('message', ({subscriptionId, timestamp, data}) => {
    if (subscriptionId in handlers) {
        handlers[subscriptionId](timestamp, data)
    }
})

// message callbacks
function onCameraImage(timestamp, data) {
    const message = messageReaders['sensor_msgs/CompressedImage'].readMessage(data)
    console.log(`Got image of ${message.data.length} bytes`)
}