#!/bin/python3

import argparse
import glob
import os.path
from typing import List, Set, Tuple


def find_msgs(search_paths: List[str]) -> List[str]:
    msg_files: Set[str] = set()
    for p in search_paths:
        msg_files = msg_files.union(set(glob.glob(p + '/**/*.msg')))
    return list(msg_files)


def read_msgs(msg_paths: List[str]) -> List[Tuple[str, str]]:
    def read(p):
        with open(p, 'r') as f:
            lines = f.readlines()
            lines = [l for l in lines if not l.startswith('#') and not l.startswith('\n')]  # prune comments
            return '\n'.join(lines)

    def get_type(p):
        return '/'.join(p.split('/')[-2:])[:-4]

    return list(zip(
        map(get_type, msg_paths),
        map(read, msg_paths),
    ))


def filter_msgs(msg_defs: List[Tuple[str, str]], exclude: List[str]) -> List[Tuple[str, str]]:
    msg_defs_new: List[Tuple[str, str]] = []
    for msg_type, msg in msg_defs:
        for pattern in exclude:
            if not msg_type.startswith(pattern):
                msg_defs_new.append((msg_type, msg))
    return msg_defs_new


def join_msgs(msg_defs: List[Tuple[str, str]]) -> str:
    msg_cat: str = ''
    for msg_type, msg in msg_defs:
        msg_cat += f'\n{"=" * 3}\nMSG: {msg_type}\n{msg}\n'
    return msg_cat


def write_template(src: str, dest: str, content: str) -> str:
    with open(src, 'r') as f1:
        with open(dest, 'w') as f2:
            f2.write(f1.read().replace('$1', content))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('search_paths', nargs='+', help='A list of paths to recursively search for message definitions')
    parser.add_argument('--exclude', nargs='+', default=[], help='A list of message types or prefixes to exclude')
    parser.add_argument('--template', type=str, default=os.path.join(os.path.dirname(__file__), '../src/generated/ros1_msgs.tpl.js'), help='Path to template to be used for generation')
    parser.add_argument('--target', type=str, default=os.path.join(os.path.dirname(__file__), '../src/generated/ros1_msgs.gen.js'), help='Path to generation output file')
    args = parser.parse_args()

    msg_paths = find_msgs(args.search_paths)
    msg_defs = read_msgs(msg_paths)
    msg_defs = filter_msgs(msg_defs, args.exclude)
    msg_cat = join_msgs(msg_defs)
    write_template(args.template, args.target, msg_cat)
