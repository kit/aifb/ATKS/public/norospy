import glob

from setuptools import setup

def remove_prefix(text: str, prefix: str) -> str:
    return text[text.startswith(prefix) and len(prefix):]

include_msgs_files = [remove_prefix(f, 'src/norospy/') for f in glob.glob('src/norospy/msg/**/*.msg')]

setup(
    include_package_data=True,
    package_data={
        'norospy': include_msgs_files,
    },
)
