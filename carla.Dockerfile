ARG ROS_DISTRO=noetic

FROM ros:$ROS_DISTRO-ros-base

ARG CARLA_VERSION=0.9.15

ENV CARLA_VERSION=${CARLA_VERSION}
ENV DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /opt/carla-ros-bridge/src
WORKDIR /opt/carla-ros-bridge

RUN apt update && \
    apt install -y curl nano python3-pip git ros-$ROS_DISTRO-foxglove-bridge ros-$ROS_DISTRO-tf ros-$ROS_DISTRO-roscpp ros-noetic-image-transport ros-noetic-image-transport-plugins ros-noetic-image-proc

COPY foxglove_bridge.launch /opt

# TODO: respect carla version
RUN cd /opt && \
    git clone -n --branch $CARLA_VERSION --depth=1 --filter=tree:0 https://github.com/carla-simulator/carla.git && \
    cd carla && \
    git sparse-checkout set --no-cone PythonAPI && \
    git checkout

RUN cd /opt && \
    git clone --recurse-submodules https://github.com/carla-simulator/ros-bridge.git && \
    mv ros-bridge/* carla-ros-bridge/src/

COPY carla_dist /opt/carla/PythonAPI/carla/dist

RUN /bin/bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash; \
    bash /opt/carla-ros-bridge/src/install_dependencies.sh; \
    if [ "$CARLA_VERSION" = "0.9.10" ] || [ "$CARLA_VERSION" = "0.9.10.1" ]; then wget https://carla-releases.s3.eu-west-3.amazonaws.com/Backup/carla-0.9.10-py2.7-linux-x86_64.egg -P /opt/carla/PythonAPI/carla/dist; fi; \
    echo "export PYTHONPATH=\$PYTHONPATH:$(ls /opt/carla/PythonAPI/carla/dist/carla-$CARLA_VERSION* | grep py$ROS_PYTHON_VERSION.)" >> /opt/carla/setup.bash; \
    echo "export PYTHONPATH=\$PYTHONPATH:/opt/carla/PythonAPI/carla" >> /opt/carla/setup.bash'

# this was broken in the official dockerfile
RUN /bin/bash -c 'echo $CARLA_VERSION > /opt/carla-ros-bridge/install/lib/carla_ros_bridge/CARLA_VERSION; \
    echo $CARLA_VERSION > /opt/carla-ros-bridge/src/carla_ros_bridge/src/carla_ros_bridge/CARLA_VERSION'

RUN /bin/bash -c 'source /opt/ros/$ROS_DISTRO/setup.bash; \
    if [ "$ROS_VERSION" == "2" ]; then colcon build; else catkin_make install; fi'

COPY --chmod=777 ros_entrypoint.sh /

# Copy custom packages and compile catkin worksapce
RUN mkdir -p /root/dev_ws/src
COPY pkgs/ /root/dev_ws/src/
RUN bash -c 'cd /root/dev_ws && source /opt/ros/$ROS_DISTRO/setup.bash && catkin_make'