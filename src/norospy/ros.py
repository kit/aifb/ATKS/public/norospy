#!/usr/bin/python3

"""
A ROS-free Python class for subscribing to ROS 1 topics without requiring a ROS distribution to be installed.
This way, you can run all ROS inside a Docker container, while still using non-outdated OS versions on your actual host.
Relies on ros-foxglove-bridge to be running as a ROS node inside the container.
"""

import glob
import json
import logging
import os
import random
import signal
import threading
from io import BytesIO
from typing import Callable, Dict, Set, Tuple, Any, Optional, List

import cv2
import numpy as np
from mcap_ros1.decoder import DecoderFactory, Schema, SchemaEncoding
from websockets.sync.client import connect

from .utils import flatten_dict, setattrpath

logging.basicConfig(level=logging.INFO)


# TODO: event callbacks -> react when advertisement was registered successfully, etc.


class ROSFoxgloveClient:
    def __init__(self, url: str = 'ws://localhost:8765', msg_search_paths: Optional[List[str]] = None):
        self.url = url

        self.msg_search_paths = (msg_search_paths or []) + [os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            './msg'
        )]

        self.websocket = None
        self.callbacks: Dict[int, Callable, Callable] = {}  # sid -> (decoder, callback)
        self.on_ready: Optional[Callable] = None
        self.topic_channels: Dict[str, int] = {}  # topic -> cid
        self.topic_channels_out: Dict[str, int] = {}  # topic -> cid
        self.topic_subscriptions: Dict[str, List[int]] = {}  # topic -> [sid]
        self.pending_subscriptions: Set[Tuple[str, str, Callable]] = set()  # first str is topic, second str is msg type
        self.pending_advertisements: Set[Tuple[str, str]] = set()  # first str is topic, second str is msg type
        self.registered_schemas: Dict[str, Schema] = {}
        self.msg_defs = self._load_msg_defs(self.msg_search_paths)
        self.decoder_factory = DecoderFactory()
        self.decoders_cache: Dict[str, Callable] = {}
        self.ready = False

    def subscribe(self, topic: str, msg_type: str, callback: Callable):
        if msg_type not in self.msg_defs:
            raise RuntimeError(f'no decoder available for "{msg_type}"')

        if not self.ready or topic not in self.topic_channels:
            self.pending_subscriptions.add((topic, msg_type, callback))
        else:
            self._subscribe(topic, msg_type, callback)

    def advertise(self, topic: str, msg_type: str):
        if msg_type not in self.msg_defs:
            raise RuntimeError(f'unregistered message type "{msg_type}"')

        if not self.ready:
            self.pending_advertisements.add((topic, msg_type))
        else:
            self._advertise(topic, msg_type)

    def publish(self, topic: str, msg: Any):
        # https://github.com/foxglove/ws-protocol/blob/main/docs/spec.md#client-message-data

        if topic not in self.topic_channels_out:
            raise RuntimeError(f'topic "{topic}" is not advertised by client')

        buf = BytesIO()
        buf.write(b'\x01')
        buf.write(int(self.topic_channels_out[topic]).to_bytes(4, 'little'))
        msg.serialize(buf)

        self.websocket.send(buf.getvalue())

    def publish_json(self, topic: str, msg_type: str, data: Dict[str, Any]):
        if msg_type not in self.registered_schemas:
            self._get_msg_decoder(msg_type)

        msg = self._get_msg_class(msg_type)()
        for k, v, in flatten_dict(data).items():
            setattrpath(msg, k, v)

        self.publish(topic, msg)

    def run(self):
        logging.info('Connecting ...')
        self.websocket = connect(self.url, subprotocols=['foxglove.websocket.v1'], max_size=None)
        logging.info('Connected.')

        for msg in self.websocket:
            if isinstance(msg, str):
                msg = json.loads(msg)

                if msg['op'] == 'advertise':
                    self._parse_channels(msg['channels'])

                    # register subscriptions
                    unsuccessful_subscriptions: Set[Tuple[str, str, Callable]] = set()
                    for r in self.pending_subscriptions:
                        try:
                            self._subscribe(*r)  # loading decoders is super slow
                            logging.info(f'Successfully subscribed to "{r[0]}"')
                        except RuntimeError as e:
                            logging.warning(f'Could not (yet) subscribe to "{r[0]}", because topic is unknown')
                            unsuccessful_subscriptions.add(r)
                    self.pending_subscriptions = self.pending_subscriptions.intersection(unsuccessful_subscriptions)

                    # register advertisements
                    for a in self.pending_advertisements:
                        self._advertise(*a)
                    self.pending_advertisements.clear()

                    if (ready := len(self.pending_subscriptions) + len(self.pending_advertisements) == 0) and not self.ready:
                        self.ready = ready
                        logging.info('ROS client fully initialized and ready to go!')
                        if self.on_ready:
                            self.on_ready()

            elif isinstance(msg, bytes):
                # https://github.com/foxglove/ws-protocol/blob/main/docs/spec.md#message-data
                if (opcode := msg[0:1]) == b'\x01':
                    sid, ts, payload = int.from_bytes(msg[1:5], 'little'), int.from_bytes(msg[5:13], 'little') / 1e9, msg[13:]
                    logging.debug(f'Got message for subscription ID {sid}')
                    if sid in self.callbacks:
                        decoder, callback = self.callbacks[sid]
                        callback(decoder(payload), ts)

    def run_background(self):
        threading.Thread(target=self.run, daemon=True).start()

    def close(self):
        logging.info('Closing ...')
        self.websocket.send(json.dumps({
            'op': 'unsubscribe',
            'subscriptionIds': list(self.callbacks.keys())
        }))
        self.websocket.send(json.dumps({
            'op': 'unadvertise',
            'channelIds': list(self.topic_channels_out.values())
        }))
        self.websocket.close()

    @staticmethod
    def parse_ros_image(image_msg: Any) -> cv2.Mat:  # TODO: type safety
        if image_msg.encoding != 'bgr8':
            raise NotImplementedError()

        data = np.frombuffer(image_msg.data, dtype=np.uint8)
        data = data.reshape((image_msg.height, image_msg.width, -1))
        # for taf data, this color conversion is not necessary -> not actually encoded as bgr8?
        data = cv2.cvtColor(data, cv2.COLOR_BGR2RGB)  # only bgra8 supported atm
        return data

    def _parse_channels(self, channels, update_subscriptions: bool = True):
        for c in channels:
            cid, topic, schema = c['id'], c['topic'], c['schemaName']
            exists: bool = topic in self.topic_subscriptions
            self.topic_channels[topic] = cid
            logging.debug(f'Registered incoming topic: {topic} ({cid})')

            if exists and update_subscriptions:
                logging.info(f'Topic {topic} was advertised again, resubscribing ...')
                for sid in [*self.topic_subscriptions[topic]]:
                    self._subscribe(topic, schema, self.callbacks[sid][1])

    def _subscribe(self, topic: str, msg_type: str, callback: Callable):
        if topic not in self.topic_channels:
            raise RuntimeError('unknown topic')

        sid: int = random.randint(0, 2 << 16)
        self.websocket.send(json.dumps({
            'op': 'subscribe',
            'subscriptions': [{'id': sid, 'channelId': self.topic_channels[topic]}]
        }))
        self.callbacks[sid] = (self._get_msg_decoder(msg_type), callback)

        if topic not in self.topic_subscriptions:
            self.topic_subscriptions[topic] = []
        self.topic_subscriptions[topic].append(sid)

    def _advertise(self, topic: str, msg_type: str):
        # https://github.com/foxglove/ws-protocol/blob/main/docs/spec.md#client-advertise

        if topic in self.topic_channels_out:
            raise RuntimeError('topic already advertised')

        channel_id: int = random.randint(0, 2 << 31)
        self.websocket.send(json.dumps({
            'op': 'advertise',
            'channels': [{
                'id': channel_id,
                'topic': topic,
                'encoding': 'ros1',
                'schemaName': msg_type
            }]
        }))
        self._get_msg_decoder(msg_type)  # register a decoder
        self.topic_channels_out[topic] = channel_id

    def _get_msg_schema(self, msg_type: str) -> Schema:
        if msg_type in self.registered_schemas:
            return self.registered_schemas[msg_type]

        msg_cat: str = self.msg_defs[msg_type]
        for msg_type_dep, msg_def in self.msg_defs.items():
            # https://github.com/foxglove/mcap/blob/734c6433b4e3ab0df403a4c41b2e6b853d6586ec/python/mcap-ros1-support/mcap_ros1/_vendor/genpy/dynamic.py#L133
            # https://github.com/foxglove/mcap/blob/734c6433b4e3ab0df403a4c41b2e6b853d6586ec/python/mcap-ros1-support/mcap_ros1/_vendor/genpy/dynamic.py#L66
            msg_cat += f'\n{"=" * 80}\nMSG: {msg_type_dep}\n{msg_def}'

        # A more efficient way to load message definitions on demand would be to use msg_loader. But is returns a MsgSpec and there is no way
        # to derive a decoder instance given a MsgSpec currently.
        # Example code:
        # sensor_msgs_image = load_msg_by_type(MsgContext.create_default(), 'sensor_msgs/Image', {
        #     'std_msgs': ['/tmp/msg/std_msgs'],
        #     'sensor_msgs': ['/tmp/msg/sensor_msgs'],
        # })

        return Schema(id=random.randint(0, int(1e5)), name=msg_type, encoding=SchemaEncoding.ROS1, data=msg_cat.encode('utf-8'))

    def _get_msg_decoder(self, msg_type: str, register: bool = True) -> Callable:
        schema: Schema = self._get_msg_schema(msg_type)
        if register:
            self.registered_schemas[msg_type] = schema
        if msg_type not in self.decoders_cache:
            self.decoders_cache[msg_type] = self.decoder_factory.decoder_for('ros1', schema)
        return self.decoders_cache[msg_type]

    def _get_msg_class(self, msg_type: str) -> Callable:
        schema: Schema = self.registered_schemas[msg_type]
        return self.decoder_factory._types[schema.id]

    @staticmethod
    def _load_msg_defs(base_dirs: List[str]) -> Dict[str, str]:
        trim_lines: Callable[[str], bool] = lambda l: len(l) > 0 and not l.startswith('#') and not l.startswith('\n')  # strip comments and empty lines

        msg_defs: Dict[str, str] = {}
        for base_dir in base_dirs:
            msg_files = glob.glob(base_dir + '/**/*.msg')
            for msg_file in msg_files:
                with open(msg_file, 'r') as f:
                    msg_text: str = '\n'.join(filter(trim_lines, f.readlines()))
                    msg_defs['/'.join(msg_file.split('/')[-2:])[:-4]] = msg_text
        logging.info(f'Loaded {len(msg_defs)} message definitions')
        return msg_defs


if __name__ == '__main__':
    # Small example application

    c = ROSFoxgloveClient('ws://localhost:8765')  # ws://129.13.90.34:8765


    def save_image(msg: Any, ts: int):  # TODO: type safety for ros messages
        filename = f'/tmp/{msg.header.frame_id.replace("/", "-")}_{ts}.jpg'
        cv2.imwrite(filename, ROSFoxgloveClient.parse_ros_image(msg))
        logging.info(f'Saved {filename}')


    def save_image_compressed(msg: Any, ts: int):  # TODO: type safety for ros messages
        filename = f'/tmp/{msg.header.frame_id.replace("/", "-")}_{ts}.jpg'
        with open(filename, 'wb') as f:
            f.write(msg.data)
        logging.info(f'Saved {filename}')


    try:
        c.run_background()
        c.subscribe('/sensors/camera/front_medium/image_raw/compressed', 'sensor_msgs/CompressedImage', save_image_compressed)
        signal.pause()
    finally:
        c.close()
