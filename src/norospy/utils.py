def setattrpath(obj, attr, value):
    """
    Set object's attribute. May use dot notation.
    Source: https://mousebender.wordpress.com/2006/11/10/recursive-getattrsetattr/
    """
    if '.' not in attr:
        setattr(obj, attr, value)
    else:
        L = attr.split('.')
        setattrpath(getattr(obj, L[0]), '.'.join(L[1:]), value)


def flatten_dict(d, parent_key='', sep='.'):
    items = []
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)
