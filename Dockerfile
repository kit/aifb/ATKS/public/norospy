ARG ROS_DISTRO=noetic

FROM ros:$ROS_DISTRO-ros-base

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y curl nano python3-pip ros-$ROS_DISTRO-foxglove-bridge ros-$ROS_DISTRO-tf ros-$ROS_DISTRO-roscpp ros-noetic-image-transport ros-noetic-image-transport-plugins ros-noetic-image-proc

# Copy scripts
COPY foxglove_bridge.launch /opt
COPY --chmod=777 ros_entrypoint.sh /

# Copy custom packages and compile catkin worksapce
RUN mkdir -p /root/dev_ws/src
COPY pkgs/ /root/dev_ws/src/
RUN ls -al /root/dev_ws/src && bash -c 'cd /root/dev_ws && source /opt/ros/$ROS_DISTRO/setup.bash && catkin_make'